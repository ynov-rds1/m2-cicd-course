FROM liliancal/ubuntu-php-apache
ADD src /var/www/
RUN apt update && \
apt install php-pear -y && \
apt install php7.4-dev -y && \
apt install libcurl3-openssl-dev -y && \
apt install unixodbc-dev -y && \
pecl install sqlsrv && \
pecl install pdo_sqlsrv

RUN printf "; priority=20\nextension=sqlsrv.so\n" > /etc/php/7.4/mods-available/sqlsrv.ini && \
printf "; priority=30\nextension=pdo_sqlsrv.so\n" > /etc/php/7.4/mods-available/pdo_sqlsrv.ini && \
phpenmod -v 7.4 sqlsrv pdo_sqlsrv && \
a2enmod mpm_prefork && \
a2enmod php7.4

RUN chmod +x /var/www/odbc.sh

EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]