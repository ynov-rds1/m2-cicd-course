# Equivalent of docker-compose for Azure Container Instances deployement.
# Use this command to deploy containers :
# az container create --resource-group myResourceGroup --file deploy-aci.yaml

apiVersion: 2021-07-01
location: francecentral
name: {{CG_NAME}}
type: Microsoft.ContainerInstance/containerGroups
properties:
  osType: Linux
  restartPolicy: Always
  containers:
  - name: cicd-course-app
    properties:
      image: registry.gitlab.com/ynov-rds1/m2-cicd-course:main
      resources:
        requests:
          cpu: 1
          memoryInGb: 1.5
      ports:
      - port: 80
      - port: 8080
  # - name: second-cicd-course-app
  #   properties:
  #     image: registry.gitlab.com/ynov-rds1/m2-cicd-course:main
  #     resources:
  #       requests:
  #         cpu: 1
  #         memoryInGb: 1.5
  #     ports:
  #     - port: 8081
  ipAddress:
    type: Public
    dnsNameLabel: {{DNS_LABEL}}
    ports:
    - protocol: tcp
      port: 80
    - protocol: tcp
      port: 8080