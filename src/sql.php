<?php

$bdd_params_object = new stdClass;
$bdd_params_object->db_host = getenv('RDS_HOSTNAME');
$bdd_params_object->db_name = getenv('RDS_DB_NAME');
$bdd_params_object->db_port = getenv('RDS_PORT');
$bdd_params_object->user = getenv('RDS_USERNAME');
$bdd_params_object->password = getenv('RDS_PASSWORD');
$bdd_params_object->connexion = 'server=';
$bdd_params_object->charset = 'utf8';

// try {
//     $conn = new PDO('sqlsrv:' . $bdd_params_object->connexion . $bdd_params_object->db_host . ';Database=' . $bdd_params_object->db_name, $bdd_params_object->user, $bdd_params_object->password);
//     $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// }
// catch (PDOException $e) {
//     echo "ERREUR CONNEXION BDD";
//     echo '<pre>';
//     print_r($bdd_params_object);
//     echo '</pre>';
//     die('Erreur : ' . $e->getMessage());
// }

// PHP Data Objects(PDO) Sample Code:
try {
    $conn = new PDO("sqlsrv:server = " . $bdd_params_object->db_host . "; Database = " . $bdd_params_object->db_name, $bdd_params_object->user, $bdd_params_object->password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    print("Error connecting to SQL Server.");
    die(print_r($e));
}

// SQL Server Extension Sample Code:
$connectionInfo = array("UID" => $bdd_params_object->user, "pwd" => $bdd_params_object->password, "Database" => $bdd_params_object->db_name, "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
$serverName = $bdd_params_object->db_host;
$conn = sqlsrv_connect($serverName, $connectionInfo);
?>